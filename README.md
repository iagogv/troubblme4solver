**trouBBlme4SolveR**


[![CRAN status](https://badges.cranchecks.info/summary/trouBBlme4SolveR.svg)](https://cran.r-project.org/web/checks/check_results_trouBBlme4SolveR.html)

`trouBBlme4SolveR` provides a function `dwmw` which deals with `lmer` and
`glmer` models warnings and messages.
